Toornament Module
=================

This module adding new field "toornament", it displays a widget toornament
in your article. You simply indicate the tournament ID in a text field.

Toornament field have not settings field, but you can adjust the size
of the widget and the first page.

For questions and proposals you can post on Drupal:
* [All issues](https://www.drupal.org/project/issues/2405211)
* [Bug report](https://www.drupal.org/project/issues/2405211?categories=1)


Installation
------------

Place the entirety of this directory in sites/all/modules/toornament.
