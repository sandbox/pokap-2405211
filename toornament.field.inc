<?php

/**
 * @file
 * Implement an toornament field, based on the file module's file field.
 */

/**
 * Implements hook_field_info().
 */
function toornament_field_info() {
  return array(
    'toornament' => array(
      'label' => t('Toornament'),
      'description' => t('This field stores the ID of an tournament.'),
      'default_widget' => 'toornament_widget',
      'default_formatter' => 'toornament_default',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function toornament_field_widget_info() {
  return array(
    'toornament_widget' => array(
      'label' => t('Toornament widget'),
      'field types' => array('toornament'),
      'settings' => array(
        'page' => 'navigator',
        'width' => 690,
        'height' => 388,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 */
function toornament_field_formatter_info() {
  return array(
    'toornament_default' => array(
      'label' => t('Toornament'),
      'field types' => array('toornament'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function toornament_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {

  if ($instance['widget']['type'] === 'toornament_widget') {
    $element['tid'] = array(
      '#type' => 'textfield',
      '#title' => t('Toornament'),
      '#description' => t('Enter the ID of a toornament. In %url, "%id" is the ID.', array(
          '%url' => 'www.toornament.com/tournaments/55213699140ba043228b456a/overview',
          '%id' => '55213699140ba043228b456a',
      )),
      '#default_value' => isset($items[$delta]['tid']) ? $items[$delta]['tid'] : NULL,
      '#size' => 24,
      '#maxlength' => 24,
    );
  }

  return $element;
}

/**
 * Implements hook_field_is_empty().
 */
function toornament_field_is_empty($item, $field) {
  return empty($item['tid']);
}

/**
 * Implements hook_field_validate().
 *
 * @see toornament_field_widget_error()
 */
function toornament_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    if (!empty($item['tid'])) {
      if (24 !== strlen($item['tid']) || !ctype_xdigit($item['tid'])) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => 'toornament_invalid',
          'message' => t('Invalid ID.'),
        );
      }
    }
  }
}

/**
 * Implements hook_field_widget_settings_form().
 */
function toornament_field_widget_settings_form($field, $instance) {
  $widget = $instance['widget'];
  $settings = $widget['settings'];

  $form = array();
  $form['page'] = array(
    '#type' => 'select',
    '#title' => t('Button page'),
    '#default_value' => $settings['page'],
    '#options' => array(
      'navigator' => t('Navigator'),
      'information' => t('Information'),
      'matches/schedule' => t('Schedule'),
      'participants'  => t('Participants'),
    ),
  );
  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#description' => t('This widget width.'),
    '#default_value' => $settings['width'],
  );
  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#description' => t('This widget height.'),
    '#default_value' => $settings['height'],
  );

  return $form;
}

/**
 * Implements hook_field_widget_error().
 *
 * @see toornament_field_validate()
 * @see form_error()
 */
function toornament_field_widget_error($element, $error, $form, &$form_state) {
  switch ($error['error']) {
    case 'toornament_invalid':
      form_error($element, $error['message']);
      break;
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function toornament_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  switch ($display['type']) {
    case 'toornament_default':
      $settings = $instance['widget']['settings'];

      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#theme' => 'toornament_widget',
          '#tournament_id' => $item['tid'],
          '#page' => $settings['page'],
          '#width' => $settings['width'],
          '#height' => $settings['height'],
        );
      }
      break;
  }

  return $element;
}

/**
 * Validation callback for a toornament id element.
 *
 * @param array $element
 *   Form element.
 * @param array &$form_state
 *   Form state.
 * @param array $form
 *   The form.
 */
function _toornament_id_validate(array $element, array &$form_state, array $form) {
  $value = $element['#value'];

  if (!empty($value) && (24 !== strlen($value) || !ctype_xdigit($value))) {
    form_error($element, t('Invalid ID.'));
  }
}
