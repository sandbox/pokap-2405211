<?php

/**
 * @file
 * Default theme implementation for display a widget.
 *
 * Available variables:
 * - $tournament_id: Identifier of tournament.
 * - $width: Widget of the widget.
 * - $height: Height of the widget.
 * - $page: The first page of widget.
 *
 * @ingroup themeable
 */
?>
<iframe width="<?php echo $width ?>"
        height="<?php echo $height ?>"
        src="//www.toornament.com/widget/tournaments/<?php echo $tournament_id ?>/<?php echo $page ?>"
        frameborder="0" allowfullscreen></iframe>
